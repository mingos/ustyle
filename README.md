# uStyle

A Stylus CSS framework.

## Construction

The framework is separated into components. Each component is independent from the others and can be loaded separately. For instance, the grid component can be used as a standalone piece of functionality, without the need to include any other components.

Some components depend on each other and when included, their dependencies will be included as well.

Each component is comprised of one or more files located in the `styl/components/<component_name>/` directory. All files are included in the correct order in the main component files: `styl/components/<component_name>.styl`. For example, the "screen" component can be used by including the file `styl/components/screen.styl` in the project's stylesheet

    @require "path/to/styl/components/screen";

## Variable overriding

Most components include files named `variables.styl`. The files contain variable declarations that will be later used in the actual CSS styling and/or for the purpose of calculating other values. These variables can be overridden by declaring them *before including the component*.

For instance, the "grid" component includes the number of columns and the width of the gutter between them. In order to set them to custom values, the following code may be used:

    $numColumns = 8;
    $gutterWidth = 30px;
    @require "path/to/styl/components/grid";

## Available components

### brand

The "brand" component defines some brand-specific features such as colours for various elements (buttons, alerts, etc.), page-wide font settings, etc.

### buttons

A set of button styles, including typical flat-styled buttons and ghost buttons, as well as button groups.

### grid

A flexible, fluid grid system. On its own, it allows the use of a fluid, non-responsive grid. If the "screen" component is included earlier in the stylesheet, "grid" provides a responsive grid system, pretty much in the spirit of the one available in Twitter Bootstrap 3.

### normalise

The "normalise" component contains CSS styles that normalise inconsistencies between different browsers and set styling rules considered to be good practices, such as setting `box-sizing` to `border-box` globally.

### screen

The "screen" component contains no styling of its own. It provides the tools for using different styles depending on the screen width.

### size

Sizes are used in other components only. This component provides uniform sizing variables.
