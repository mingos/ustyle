module.exports = function(grunt) {
	grunt.initConfig({
		stylus: {
			dev: {
				options: {
					compress: false
				},
				files: {
					"dev/style.css": [
						"styl/style.styl"
					]
				}
			},
			dist: {
				options: {
					cleancss: true
				},
				files: {
					"dist/style.css": [
						"styl/style.styl"
					]
				}
			}
		},
		watch: {
			dev: {
				files: ["styl/**/*.styl"],
				tasks: ["dev"]
			}
		}
	});

	// Load tasks
	grunt.loadNpmTasks("grunt-contrib-stylus");
	grunt.loadNpmTasks("grunt-contrib-watch");

	// Custom tasks
	grunt.registerTask("default", []);

	grunt.registerTask("dev", [
		"stylus:dev"
	]);
	grunt.registerTask("dist", [
		"stylus:dist"
	]);
};
